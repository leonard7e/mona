# Mona

Mona is a theme, usable in Grav.

##### Installation

On Ubuntu, do
> apt-get install nodejs npm

This will give you npm.

> cd <path>/mona
> npm install bower

Finally do
> node_modules/bower/bin/bower install

This will install Monas dependencies locally.  

After this, Mona is ready to use.

Enjoy.
